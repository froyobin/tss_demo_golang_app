package keygen

import (
	"gotest.tools/assert"
	"testing"
)


func TestConvertfromTsspubkey(t *testing.T){
	x := "47137027989692955970746856944362466889300538230670630691023471295987574986470"
	y := "34580233108674282612423198566421674550760888834420154878640966449230453091247"


	address, _ := convertfromTsspubkey(x,y)

	assert.Equal(t,address.String(), "tbnb132jwqdwqt20sy7a7z5j9nr8crttlnssry4vrxk")

}
