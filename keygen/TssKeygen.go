package keygen

import (
	"bytes"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"github.com/binance-chain/go-sdk/common/types"
	sdkkey "github.com/binance-chain/go-sdk/keys"
	"github.com/btcsuite/btcd/btcec"
	"github.com/tendermint/tendermint/crypto/secp256k1"
	"io/ioutil"
	"math/big"
	"net/http"
	"path"
	"strings"
)

type PostValue struct {
	PubkeyArrayStr  string
	AddressArrayStr string
	Privkey         string
}

func loadaddresses(directorypath string, partynum int) (string, string) {
	addressesstrarray := ""
	pubkeystrarray := ""
	for i := 1; i < partynum+1; i++ {
		filename := fmt.Sprintf("/keystore%d.json", i)
		filepath := path.Join(directorypath, filename)
		//fixme we prepared some test keystores and the password is "aabb" as default.
		key, _ := sdkkey.NewKeyStoreKeyManager(filepath, "aabb")
		pubkey := key.GetPrivKey().PubKey().Bytes()
		addrstr := base64.StdEncoding.EncodeToString(key.GetPrivKey().PubKey().Address().Bytes())
		pubkeystr := base64.StdEncoding.EncodeToString(pubkey)
		addressesstrarray += addrstr + ","
		pubkeystrarray += pubkeystr + ","
	}
	addressesstrarray = strings.TrimSuffix(addressesstrarray, ",")
	pubkeystrarray = strings.TrimSuffix(pubkeystrarray, ",")

	return addressesstrarray, pubkeystrarray
}

// Runkeygen is the function to generate Tss keys.
func Runkeygen(keyfiledirectory, mykeystore string, partynum int, url string) bool {
	types.Network = types.TestNetwork

	addressesstrarray, pubkeystrarray := loadaddresses(keyfiledirectory, partynum)
	mykey, _ := sdkkey.NewKeyStoreKeyManager(mykeystore, "aabb")
	privkeystr, _ := mykey.ExportAsPrivateKey()

	base64key := base64.StdEncoding.EncodeToString([]byte(privkeystr))
	postvalue := PostValue{
		pubkeystrarray,
		addressesstrarray,
		base64key,
	}
	jsonValue, _ := json.Marshal(postvalue)
	fmt.Println("keg gen talk------>", url)
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(jsonValue))
	if err != nil {
		return false
	}
	// Read Response Body
	respbody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
		return false
	}

	if resp.StatusCode != 200 {
		fmt.Println("error in get the response from Tss keygen error code: ", resp.StatusCode)
		return false
	}
	var dat map[string]interface{}
	err = json.Unmarshal(respbody, &dat)
	if err != nil {
		return false
	}

	Tssretmap := dat["Ok"].(map[string]interface{})

	address, err := convertfromTsspubkey(Tssretmap["Pubkeyx"].(string), Tssretmap["Pubkeyy"].(string))
	fmt.Println(address)
	return true
}

func convertfromTsspubkey(x, y string) (types.AccAddress, error) {
	types.Network = types.TestNetwork
	Pubx, _ := new(big.Int).SetString(x, 10)
	Puby, _ := new(big.Int).SetString(y, 10)

	tsspubkey := btcec.PublicKey{
		btcec.S256(),
		Pubx,
		Puby,
	}

	var pubkeyObj secp256k1.PubKeySecp256k1
	copy(pubkeyObj[:], tsspubkey.SerializeCompressed())
	address, err := types.AccAddressFromHex(hex.EncodeToString(pubkeyObj.Address()))
	return address, err
}
