package threadsend

import (
	"encoding/json"
	"fmt"
	"github.com/GiterLab/urllib"
	sdkclient "github.com/binance-chain/go-sdk/client"
	"github.com/binance-chain/go-sdk/common/types"
	sdkcommon "github.com/binance-chain/go-sdk/common/types"
	sdkkey "github.com/binance-chain/go-sdk/keys"
	sdkmsg "github.com/binance-chain/go-sdk/types/msg"
	"github.com/binance-chain/go-sdk/types/tx"
	"reflect"
	"runtime"
	"strings"
	"sync"
	"time"
)

const tasknum = 1

type HexTxPack struct {
	HexTx       []byte
	currenttime int64
}

func addJob(jobs chan<- int64, num int, baseseq int64) {

	for i := 0; i < num; i++ {
		jobs <- int64(i) + baseseq
	}
	close(jobs)

}

func doJob(
	client sdkclient.DexClient, HexTxPool map[int64]HexTxPack, jobs <-chan int64,
	transactionLocker sync.RWMutex, wg *sync.WaitGroup, worker int, recv string, accnum, amount int64) {
	defer wg.Done()
	for seq := range jobs {
		err := docreateandsend(HexTxPool, recv, client, seq, accnum,amount, transactionLocker)
		if err == nil {
			continue
		}
	}
}

func QueryTransaction(transactions map[string]int64) map[string]int64 {

	statisticDic := make(map[string]int64)

	for HTX, _ := range transactions {

		urlstr := fmt.Sprintf("https://testnet-explorer.binance.org/tx/%s", HTX)
		retstring, _ := urllib.Get(urlstr).String()
		index := strings.Index(retstring, "{\"txHash\"")
		if index < 0 {
			statisticDic[HTX] = -1
			continue
		}
		timestamp := retstring[index:]
		indexend := strings.Index(timestamp, "}")

		rawjsontime := timestamp[:indexend+1]

		var data map[string]interface{}
		err := json.Unmarshal([]byte(rawjsontime), &data)
		if err != nil {
			panic(err)
		}

		confirmedtimeinterface := data["timeStamp"]

		v := reflect.ValueOf(confirmedtimeinterface)
		var floatType = reflect.TypeOf(float64(0))
		confirmedtime := v.Convert(floatType).Float()
		statisticDic[HTX] = int64(confirmedtime)

		//we cannot query the website that frequently otherwise we have hit the query limitation
		time.Sleep(time.Microsecond * 300)

	}

	return statisticDic

}

func Sendcoinexample(url, address string, amount int64) {
	types.Network = types.TestNetwork
	sdkkey.SIGNERADDR = url
	var transactionLocker sync.RWMutex
	var submitLocker sync.RWMutex
	var wg sync.WaitGroup
	var wgsubmission sync.WaitGroup

	receiverpool := []string{"tbnb1qsew48an2zkqs3qfxsacz06qc0atvtaknp6kqc",
		"tbnb1y5u2y586yr2fgt8rhzuxrqwsekxhjsqnp2prwy"}

	Tsssenderaddr := address

	TssSender, err := sdkkey.NewTssKeyKeyManager(Tsssenderaddr)

	if err != nil {
		panic(err)
	}

	Mainclient, err := sdkclient.NewDexClient("testnet-dex.binance.org", types.TestNetwork, TssSender)
	acc, err := Mainclient.GetAccount(Tsssenderaddr)
	if err != nil {
		fmt.Println(err)
		return
	}
	accnum := acc.Number
	var worker = runtime.NumCPU()
	done := false
	//we also need to wait for the post threads
	var totalwait int
	if tasknum < worker {
		worker = tasknum
	}
	//we add the extra thread for submitting the transactions.
	totalwait = worker
	wg.Add(totalwait)
	wgsubmission.Add(1)
	jobs := make(chan int64, worker)
	counter := make(chan int, worker)
	transactionchannel := make(map[string]int64)
	HexTxPool := make(map[int64]HexTxPack)

	time.Sleep(time.Second * 2)

	go addJob(jobs, tasknum, acc.Sequence)

	//go for the statistic of the transactions

	go func() {
		for done == false {
			<-counter
		}
	}()

	for i := 0; i < worker; i++ {
		go doJob(Mainclient, HexTxPool, jobs, transactionLocker, &wg, i, receiverpool[0], accnum, amount)
	}

	go submittrans(acc.Sequence, Mainclient, HexTxPool, transactionchannel, &wgsubmission, &wg, submitLocker, transactionLocker)

	wgsubmission.Wait()

}

func submittrans(startseq int64, client sdkclient.DexClient, HexTxPool map[int64]HexTxPack,
	transactionchannel map[string]int64, wgsubmission, wg *sync.WaitGroup, submitLocker, transactionLocker sync.RWMutex) {

	defer wgsubmission.Done()
	wg.Wait()
	//time.Sleep(time.Second*10)
	fmt.Println("\n now we are ready to submit the transactions...")
	waitercount := 0
	seqindex := startseq
	for i := 0; i < tasknum; i++ {

		transactionLocker.RLock()
		pack, exist := HexTxPool[seqindex]
		transactionLocker.RUnlock()
		if exist {
			Hextx := pack.HexTx
			currenttime := pack.currenttime
			ret, err := client.PostToken(Hextx, true)
			if err != nil {
				fmt.Printf(err.Error())
				continue
			}
			fmt.Println(ret)
			submitLocker.Lock()
			transactionchannel[ret.Hash] = currenttime
			fmt.Println("transaction submitted", ret)
			submitLocker.Unlock()
			seqindex += 1

		} else {
			// seems this transaction is not ready, we wait
			time.Sleep(time.Microsecond * 1000)
			waitercount += 1
			i -= 1
			if waitercount > 100 {
				fmt.Printf("we panic here since we cannot find the expected transaction with too many errors")
				return
			}
			continue
		}
	}

}

func docreateandsend(HexTxPool map[int64]HexTxPack, receiver string, client sdkclient.DexClient,
	seq, accnumber int64, amount int64, transactionLocker sync.RWMutex) error {

	coin1 := sdkcommon.Coin{
		"BNB",
		amount,
	}

	coins := make([]sdkcommon.Coin, 0)

	coins = append(coins, coin1)

	recv, err := sdkcommon.AccAddressFromBech32(receiver)
	if err != nil {
		pack := HexTxPack{
		}
		transactionLocker.Lock()
		HexTxPool[-1] = pack
		transactionLocker.Unlock()
		panic("receiver address error!!")
	}

	msgs := make([]sdkmsg.Transfer, 0)

	msg := sdkmsg.Transfer{
		ToAddr: recv,
		Coins:  coins,
	}

	msgs = append(msgs, msg)

	currenttime := time.Now().UnixNano()
	//we convert nanoseconds to milliseconds
	currenttime = int64(currenttime / 1000000)

	Hextx, err := client.PrepareToken(msgs, true, func(signmsg *tx.StdSignMsg) *tx.StdSignMsg {
		signmsg.Sequence = seq
		signmsg.AccountNumber = accnumber
		return signmsg

	})

	if err != nil {
		fmt.Printf(err.Error())
		pack := HexTxPack{
		}
		transactionLocker.Lock()
		HexTxPool[-1] = pack
		transactionLocker.Unlock()
		return err
	}

	pack := HexTxPack{
		Hextx,
		currenttime,
	}

	transactionLocker.Lock()
	HexTxPool[seq] = pack
	transactionLocker.Unlock()
	fmt.Printf("add transactions\n")

	return nil
}
