package tsssign

import (
	restfulapisignsend "TSS_DEMO/restfulapisign"

	"github.com/binance-chain/go-sdk/common/types"
	sdkkey "github.com/binance-chain/go-sdk/keys"
)

//Runsigner run the statechain signer
func Runsigner(url, address string) {
	types.Network = types.TestNetwork

	sdkkey.SIGNERADDR = url

	restfulapisignsend.Run(address)

}
