// This program is just for test purpose, the variable
// since Binance chain needs the transactions that submitted follow the strict sequence. If one fails, the rest
// need to redo the signature generation. Luckily, the signature generation is controlled by us.

package main

import (
	"TSS_DEMO/keygen"
	"TSS_DEMO/threadsend"
	"TSS_DEMO/tsssign"
	"flag"
)

func main() {

	var mykeystore = flag.String("key", "", "The keyfile of this node")
	var keyfiledirectory = flag.String("l", "./", "The location of key files")
	var partynum = flag.Int("n", 4, "number of the parties in Tss")
	var optionsign = flag.Bool("s", false, "run the sign function")
	var optionkeygen = flag.Bool("k", false, "run the keygen function")
	var optionexample = flag.Bool("e", false, "send token example")
	var url = flag.String("u", "http://127.0.0.1:8323/recvmsg", "url of the tss service")
	var address = flag.String("a", "tbnb1mv6tyrgfd46qdsh5kvzp9gqse5fry8lk0ny6h3", "sender address")
	var amount = flag.Int64("m", 1, "amount of bnb that send (should times 10^8)")

	flag.Parse()

	if *optionsign == true {
		tsssign.Runsigner(*url, *address)
	}

	if *optionkeygen == true {
		keygen.Runkeygen(*keyfiledirectory, *mykeystore,*partynum, *url)
	}

	if *optionexample == true {

		threadsend.Sendcoinexample(*url, *address, *amount)
	}
	return

}
