package mock

import (
	"encoding/json"
	"io/ioutil"
	"sort"
	"strconv"
	"strings"

	sdkclient "github.com/binance-chain/go-sdk/client"
	"github.com/binance-chain/go-sdk/common/types"
	sdkcommon "github.com/binance-chain/go-sdk/common/types"
	sdkkey "github.com/binance-chain/go-sdk/keys"
	sdkmsg "github.com/binance-chain/go-sdk/types/msg"
	"github.com/binance-chain/go-sdk/types/tx"
	"github.com/op/go-logging"

	"math/rand"
	"os"
	"sync"
	"time"
)

type HexTxPack struct {
	HexTx       []byte
	currenttime int64
}

type Transaction struct {
	Height  string `json:"height"`
	Hash    string `json:"hash"`
	TxArray []struct {
		To     string `json:"to"`
		Amount string `json:"amount"`
		Token  string `json:"token"`
	} `json:"tx_array"`
}

type ErrorMSg struct {
	Code             int           `json:"code"`
	FailedTxIndex    int           `json:"failed_tx_index"`
	Message          string        `json:"message"`
	SuccessTxResults []interface{} `json:"success_tx_results"`
}

type Transactions []*Transaction

type ByHeight struct{ Transactions }

func (s Transactions) Len() int      { return len(s) }
func (s Transactions) Swap(i, j int) { s[i], s[j] = s[j], s[i] }

var log = logging.MustGetLogger("mock")

func init() {
	// This call is for testing purposes and will set the time to unix epoch.

	var format = logging.MustStringFormatter(
		`%{color}%{time:15:04:05.000} %{shortfile} ▶ %{level:.4s} %{color:reset} %{message}`,
	)

	// For demo purposes, create two backend for os.Stderr.
	backend1 := logging.NewLogBackend(os.Stdout, "", 0)

	backend1Formatter := logging.NewBackendFormatter(backend1, format)
	// Only errors and more severe messages should be sent to backend1
	backend1Leveled := logging.AddModuleLevel(backend1Formatter)
	backend1Leveled.SetLevel(logging.DEBUG, "")

	// Set the backends to be used.
	logging.SetBackend(backend1Leveled)

}

func Loaddata() Transactions {
	dat, err := ioutil.ReadFile("mockdata.json")
	if err != nil {

		log.Error("error in open file", err)
	}

	var transactions Transactions
	json.Unmarshal(dat, &transactions)

	return transactions
}

func docreateandsend(HexTxPool map[int64]HexTxPack, transaction Transaction, client sdkclient.DexClient, seq, accnum int64) error {

	coins := make([]sdkcommon.Coin, 0)
	msgs := make([]sdkmsg.Transfer, 0)

	for _, each := range transaction.TxArray {

		amount, err := strconv.ParseInt(each.Amount, 10, 64)
		if err != nil {
			log.Error("%d of type %T", amount, amount)
		}

		coin1 := sdkcommon.Coin{
			each.Token,
			amount,
		}
		coins = append(coins, coin1)
		recv, err := sdkcommon.AccAddressFromBech32(each.To)

		if err != nil {
			log.Error("error in parse the address")
			return err
		}
		msg := sdkmsg.Transfer{
			ToAddr: recv,
			Coins:  coins,
		}
		msgs = append(msgs, msg)

	}

	currenttime := time.Now().UnixNano()
	//we convert nanoseconds to milliseconds
	currenttime = int64(currenttime / 1000000)

	Hextx, err := client.PrepareToken(msgs, true, func(signmsg *tx.StdSignMsg) *tx.StdSignMsg {
		signmsg.Sequence = seq
		signmsg.AccountNumber = accnum
		return signmsg

	})

	if err != nil {
		log.Error(err.Error())
		pack := HexTxPack{}
		HexTxPool[-1] = pack
		return err
	}

	pack := HexTxPack{
		Hextx,
		currenttime,
	}

	HexTxPool[seq] = pack

	return nil
}

func submittrans(startseq int64, client sdkclient.DexClient, HexTxPool map[int64]HexTxPack,
	transactionchannel map[string]int64) bool {

	seqindex := startseq

	pack, _ := HexTxPool[seqindex]
	Hextx := pack.HexTx
	currenttime := pack.currenttime
	ret, err := client.PostToken(Hextx, true)
	if err != nil {
		var errstr string
		errstr = err.Error()
		if len(errstr) == 0 {
			log.Debug("err message is none")
			return true

		}
		pos := strings.Index(errstr, "response:")
		if pos == -1 {
			log.Error("transaction submitted error", err)
			return false
		}
		handle := errstr[pos:]
		pos2 := strings.Index(handle, " {")
		if pos2 == -1 {
			log.Error("transaction submitted error", err)

			return false
		}

		var errmsg ErrorMSg
		json.Unmarshal([]byte(handle[pos2:]), &errmsg)
		if errmsg.Code == 500{
			log.Debug("Duplicated submission")
			return true
		}

		log.Error("transaction submitted error", err)
		return false
	}
	transactionchannel[ret.Hash] = currenttime
	log.Debug("transaction submitted", ret)
	seqindex += 1

	return true
}

func processtx(tx Transaction, Mainclient sdkclient.DexClient, seq, accnum int64) bool {

	HexTxPool := make(map[int64]HexTxPack)
	transactionchannel := make(map[string]int64)

	docreateandsend(HexTxPool, tx, Mainclient, seq, accnum)
	//ret := true
	ret := submittrans(seq, Mainclient, HexTxPool, transactionchannel)
	return ret
}

func (s ByHeight) Less(i, j int) bool {

	s1, err := strconv.ParseInt(s.Transactions[i].Height, 10, 64)
	if err != nil {
		log.Error("%d of type %T", s1, s1)
	}

	s2, err := strconv.ParseInt(s.Transactions[j].Height, 10, 64)
	if err != nil {
		log.Error("%d of type %T", s2, s2)
	}

	return s1 < s2

}

func Mock() {
	transactions := Loaddata()

	//we ensure all the nodes have the same order of the transactions.
	sort.Sort(ByHeight{transactions})

	s1 := rand.NewSource(time.Now().UnixNano())
	r1 := rand.New(s1)
	var wg sync.WaitGroup
	wg.Add(2)
	sendchan := make(chan Transaction)

	Tsssenderaddr := "tbnb17fy57lu2uz2cd6e3x9a43xlc2a48gq0deckxgd"

	TssSender, err := sdkkey.NewTssKeyKeyManager(Tsssenderaddr)

	if err != nil {
		panic(err)
	}

	Mainclient, err := sdkclient.NewDexClient("testnet-dex.binance.org", types.TestNetwork, TssSender)
	acc, err := Mainclient.GetAccount(Tsssenderaddr)
	if err != nil {
		log.Error(err)
		return
	}
	accnum := acc.Number

	//this is the signer that send to the tss interface
	go func() {
		for _, eachtx := range transactions {
			value := r1.Int() % 500
			sendchan <- *eachtx
			time.Sleep(time.Microsecond * time.Duration(value))
		}
		close(sendchan)
		wg.Done()
	}()

	//this is the tss interface that process the transactions
	go func() {
		seq := acc.Sequence
		for {
			tx, ok := <-sendchan
			if ok {
				if processtx(tx, Mainclient, seq, accnum) == false {
					log.Error("faild to submit the transaction")
					continue
				}
				seq += 1
			} else {
				break
			}
		}
		wg.Done()
	}()

	wg.Wait()

}
