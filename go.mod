module binance_chain

go 1.12

require (
	github.com/binance-chain/go-sdk v1.0.8
	github.com/stretchr/testify v1.3.0
)

replace github.com/tendermint/go-amino => github.com/binance-chain/bnc-go-amino v0.14.1-binance.1
