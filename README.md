# This repo contains three demos
### 1. Tss key generation
- Firstly run the **docker-compose-tss-keygen.yml** to start the docker key generation environment
 
- prepare some keystore files as the identity of the signers that join this key gen phase.(by default, I put 4 keystores in the repo and the password is **aabb**).
  
- Then run the script **run_keygen.sh** you can modify the parameters in **run_keygen.sh** to test with different party number and threshold(you shold have the same number of Tss nodes in docker accordingly.)


docker-compose-tss-pure.yml

## 2 Tss send token example
- Firstly run the **docker-compose-tss-prune.yml** to start the docker key generation environment
 
- Then run the script **run_sign_example.sh** you can modify the parameters to test with different party number and threshold(you shold have the same number of Tss nodes in docker accordingly.)

- check on the Binance explorer to find the transactions on the Binance chain.


## 3 Tss statechain example

- Firstly run the **docker-compose-tss-keygen.yml** to start the docker key generation environment
 
- Then run the script **run_sign_statechain.sh** you can modify the parameters to test with different party number and threshold(you shold have the same number of Tss nodes in docker accordingly.)

By default, I make the scanner to scan the statechain from the block 73320.


# **Please remember to backup your Tss keyfile before running the Tss keygen, otherwise you will have no way to recover them!!!**